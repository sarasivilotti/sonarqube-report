<?xml version="1.0" encoding="utf-8"?>

<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


  <!-- defines the layout master -->
  <fo:layout-master-set>
    <fo:simple-page-master master-name="first"
                           page-height="29.7cm"
                           page-width="21cm"
                           margin-top="1cm"
                           margin-bottom="2cm"
                           margin-left="2.5cm"
                           margin-right="2.5cm">
      <fo:region-body margin-top="3cm"/>
      <fo:region-before extent="3cm"/>
      <fo:region-after extent="1.5cm"/>
    </fo:simple-page-master>
  </fo:layout-master-set>

  <!-- starts actual layout -->
  <fo:page-sequence master-reference="first">

  <fo:flow flow-name="xsl-region-body">

      <fo:block space-after.optimum="35pt" padding-top="3pt">
      <!-- table start --> 
       <fo:table table-layout="fixed" width="100%" border-collapse="separate">
        <fo:table-column column-width="40%"/>
      	<fo:table-column column-width="30%"/>
      	<fo:table-column column-width="30%"/>
      	<fo:table-body>
      	 <fo:table-row>
            <fo:table-cell margin-left="3pt" border-width="0.2mm" border-style="solid" border-color="black" number-columns-spanned="2"><fo:block>Source Code Analysis Report</fo:block></fo:table-cell>
            <fo:table-cell margin-right="3pt" border-width="0.2mm" border-style="solid" border-color="black" text-align="right"><fo:block><xsl:value-of select="Report/TodayDate"/></fo:block></fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell margin-left="3pt" border-width="0.2mm" border-style="solid" border-color="black"><fo:block><fo:external-graphic src="url('.\files\LogoSpindox.jpg')" width="50%" content-width="scale-to-fit"/></fo:block></fo:table-cell>
            <fo:table-cell margin-right="3pt" border-width="0.2mm" border-style="solid" border-color="black" text-align="right"><fo:block>Powered by <fo:external-graphic src=".\files\LogoSonar.jpg" width="75%" content-width="scale-to-fit"/></fo:block></fo:table-cell>
          	<fo:table-cell margin-left="3pt" margin-right="3pt" border-width="0.2mm" border-style="solid" border-color="black" text-align="right"><fo:block><fo:external-graphic src="file:.\files\LogoTIM.png" width="65%" content-width="scale-to-fit"/></fo:block></fo:table-cell>
          </fo:table-row>
        </fo:table-body>
       </fo:table>
      <!-- table end --> 
 	  </fo:block>

      <fo:block font-size="14pt"
            font-family="sans-serif"
            space-after.optimum="15pt"
            text-align="center">
        Source Code Analysis Report
      </fo:block>
     
     <fo:block space-after.optimum="30pt" margin-left="10pt">
     <!-- table start --> 
      <fo:table table-layout="fixed" width="100%" border-collapse="separate">
      	<fo:table-column column-width="30%"/>
      	<fo:table-column column-width="70%"/>
      	<fo:table-body>
          <fo:table-row>
            <fo:table-cell border-width="0.2mm" border-style="solid" border-color="black"><fo:block padding="4pt">Nome Progetto</fo:block></fo:table-cell>
            <fo:table-cell border-width="0.2mm" border-style="solid" border-color="black"><fo:block padding="4pt" font-size="10pt"><xsl:value-of select="Report/ProjectName"/></fo:block></fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell border-width="0.2mm" border-style="solid" border-color="black"><fo:block padding="4pt">Analisi del</fo:block></fo:table-cell>
            <fo:table-cell border-width="0.2mm" border-style="solid" border-color="black"><fo:block padding="4pt" font-size="10pt"><xsl:value-of select="Report/AnalysesDate"/></fo:block></fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell border-width="0.2mm" border-style="solid" border-color="black"><fo:block padding="4pt">Version</fo:block></fo:table-cell>
            <fo:table-cell border-width="0.2mm" border-style="solid" border-color="black"><fo:block padding="4pt" font-size="10pt"><xsl:value-of select="Report/Version"/></fo:block></fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell border-width="0.2mm" border-style="solid" border-color="black">
            	<fo:block padding="4pt">
            	Dimensioni (LOC*)
            	</fo:block>
            	<fo:block padding="4pt" font-size="7pt">
            	(*) Lines Of Code
            	</fo:block>
            </fo:table-cell>
            <fo:table-cell border-width="0.2mm" border-style="solid" border-color="black"><fo:block padding="4pt" font-size="10pt"><xsl:value-of select="Report/LOC"/></fo:block></fo:table-cell>
          </fo:table-row>
    	</fo:table-body>
      </fo:table>
     <!-- table end --> 
     </fo:block>
     
     <fo:block space-after.optimum="30pt" margin-left="10pt">
     <!-- table start --> 
      <fo:table table-layout="fixed" width="100%" border-collapse="separate">
      	<fo:table-column column-width="60%"/>
      	<fo:table-column column-width="30%"/>
      	<fo:table-column column-width="10%"/>
      	<fo:table-body>
          <fo:table-row>
            <fo:table-cell border-width="0.2mm" font-size="9pt" border-style="solid" border-color="black" border-right-color="white">
            	<fo:block font-size="9pt" text-align="start" space-after.optimum="5pt" padding="10pt">
     			<fo:inline font-weight="bold">Robustezza:</fo:inline> 
     			riporta il numero delle Issues
				legate alla robustezza (Bugs) riscontrate nel
				codice ed il Reliability Rating
    			</fo:block>
            </fo:table-cell>
            <fo:table-cell margin-left="3pt" border-width="0.2mm" text-align="center" border-style="solid" border-color="black" border-left-color="white">
            	<fo:block><xsl:value-of select="Report/Bugs/Total"/>
            	
            	<xsl:if test="Report/Bugs/BugsRating = 'A'">
      				<fo:external-graphic src="files/RateA.png" width="15%" content-width="scale-to-fit"/>      				      	      				
   				</xsl:if>
   				<xsl:if test="Report/Bugs/BugsRating = 'B'">
      				<fo:external-graphic src="files/RateB.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
   				<xsl:if test="Report/Bugs/BugsRating = 'C'">
      				<fo:external-graphic src="files/RateC.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
   				<xsl:if test="Report/Bugs/BugsRating = 'D'">
      				<fo:external-graphic src="files/RateD.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
   				<xsl:if test="Report/Bugs/BugsRating = 'E'">
      				<fo:external-graphic src="files/RateE.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
            	</fo:block>
            	
            	<fo:block><fo:external-graphic src="files/Bugs.png" width="40%" content-width="scale-to-fit"/></fo:block>
            </fo:table-cell>
            <fo:table-cell number-rows-spanned="3">
     			<fo:block><fo:external-graphic src="files/Rating.png" width="100%" content-width="scale-to-fit"/></fo:block>
    		</fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell border-width="0.2mm" border-style="solid" border-color="black" border-right-color="white">
            	<fo:block font-size="9pt" text-align="start" space-after.optimum="5pt" padding="10pt">
     			<fo:inline font-weight="bold">Sicurezza:</fo:inline>
     			riporta il numero delle Issues
				legate alla sicurezza (Vulnerabilities)
				riscontrate nel codice ed il Security Rating
    			 </fo:block>
            </fo:table-cell>
            <fo:table-cell margin-left="3pt" border-width="0.2mm" text-align="center" border-style="solid" border-color="black" border-left-color="white">
            	<fo:block><xsl:value-of select="Report/Vulnerabilities/Total"/>
            	
            	<xsl:if test="Report/Vulnerabilities/VulnerabilitiesRating = 'A'">
      				<fo:external-graphic src="files/RateA.png" width="15%" content-width="scale-to-fit"/>      				
   				</xsl:if>
   				<xsl:if test="Report/Vulnerabilities/VulnerabilitiesRating = 'B'">
      				<fo:external-graphic src="files/RateB.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
   				<xsl:if test="Report/Vulnerabilities/VulnerabilitiesRating = 'C'">
      				<fo:external-graphic src="files/RateC.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
   				<xsl:if test="Report/Vulnerabilities/VulnerabilitiesRating = 'D'">
      				<fo:external-graphic src="files/RateD.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
   				<xsl:if test="Report/Vulnerabilities/VulnerabilitiesRating = 'E'">
      				<fo:external-graphic src="files/RateE.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
            	</fo:block>
            	
            	<fo:block><fo:external-graphic src="files/Vulnerabilities.png" width="50%" content-width="scale-to-fit"/></fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell border-width="0.2mm" border-style="solid" border-color="black" border-right-color="white">
            	<fo:block font-size="9pt" text-align="start" space-after.optimum="20pt" padding="10pt">
     			<fo:inline font-weight="bold">Manutenibilità:</fo:inline>
     			riporta il numero delle Issues
				legate alla manutenibilità (Code Smells)
				riscontrate nel codice ed il Maintainability Rating
    			</fo:block>
            </fo:table-cell>
            <fo:table-cell margin-right="3pt" border-width="0.2mm" text-align="center" border-style="solid" border-color="black" border-left-color="white">
            	<fo:block><xsl:value-of select="Report/CodeSmells/Total"/>
            	
            	<xsl:if test="Report/CodeSmells/CSRating = 'A'">
      				<fo:external-graphic src="files/RateA.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
   				<xsl:if test="Report/CodeSmells/CSRating = 'B'">
      				<fo:external-graphic src="files/RateB.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
   				<xsl:if test="Report/CodeSmells/CSRating = 'C'">
      				<fo:external-graphic src="files/RateC.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
   				<xsl:if test="Report/CodeSmells/CSRating = 'D'">
      				<fo:external-graphic src="files/RateD.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
   				<xsl:if test="Report/CodeSmells/CSRating = 'E'">
      				<fo:external-graphic src="files/RateE.png" width="15%" content-width="scale-to-fit"/>
   				</xsl:if>
            	</fo:block>
            	
            	<fo:block><fo:external-graphic src="files/CodeSmells.png" width="50%" content-width="scale-to-fit"/></fo:block>
            </fo:table-cell>
          </fo:table-row>
    	</fo:table-body>
      </fo:table>
     <!-- table end --> 
     </fo:block>
                  
    <!-- normal text -->
    <fo:block text-align="start" space-after.optimum="5pt">
    	Distribuzione Issues per tipologie e severity:
    </fo:block>

	<fo:block>
    <!-- table start -->
    <fo:table table-layout="fixed" width="100%" border-collapse="separate" margin-left="3pt">
      <fo:table-column column-width="32mm"/>
      <fo:table-column column-width="32mm"/>
      <fo:table-column column-width="32mm"/>
      <fo:table-column column-width="32mm"/>
      <fo:table-column column-width="32mm"/>
      <fo:table-body>
        <fo:table-row>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue"><fo:block>Issues</fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block>Bugs</fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block>Vulnerabilities</fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block>Code Smells</fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block>Totale</fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="red"><fo:block>Blocker</fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/Bugs/Blocker"/></fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/Vulnerabilities/Blocker"/></fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/CodeSmells/Blocker"/></fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block><xsl:value-of select="Report/Total/TotalBlocker"/></fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="orange"><fo:block>Critical</fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/Bugs/Critical"/></fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/Vulnerabilities/Critical"/></fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/CodeSmells/Critical"/></fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block><xsl:value-of select="Report/Total/TotalCritical"/></fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="yellow"><fo:block>Major</fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/Bugs/Major"/></fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/Vulnerabilities/Major"/></fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/CodeSmells/Major"/></fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block><xsl:value-of select="Report/Total/TotalMajor"/></fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="YellowGreen"><fo:block>Minor</fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/Bugs/Minor"/></fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/Vulnerabilities/Minor"/></fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/CodeSmells/Minor"/></fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block><xsl:value-of select="Report/Total/TotalMinor"/></fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="green"><fo:block>Info</fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/Bugs/Info"/></fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/Vulnerabilities/Info"/></fo:block></fo:table-cell>
          <fo:table-cell border-width="0.2mm" border-style="solid" border-color="white" background-color="lightblue" text-align="center"><fo:block><xsl:value-of select="Report/CodeSmells/Info"/></fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block><xsl:value-of select="Report/Total/TotalInfo"/></fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue"><fo:block>Totale</fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block><xsl:value-of select="Report/Bugs/Total"/></fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block><xsl:value-of select="Report/Vulnerabilities/Total"/></fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block><xsl:value-of select="Report/CodeSmells/Total"/></fo:block></fo:table-cell>
          <fo:table-cell color="white" border-width="0.2mm" border-style="solid" border-color="white" background-color="RoyalBlue" text-align="center"><fo:block><xsl:value-of select="Report/Total/Total"/></fo:block></fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
    <!-- table end -->
    </fo:block>

    </fo:flow>
  </fo:page-sequence>
</fo:root>
