package com.spindox.SonarReport;

import org.apache.commons.codec.binary.Base64;

import java.net.*;
import java.text.SimpleDateFormat;
import java.io.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BackgroundTask {

	public void ReportSuccess(String s, String p, String dir) throws IOException, ParseException {
		
		final Logger logger = LoggerFactory.getLogger(BackgroundTask.class);
		
		final String CONTENT_TYPE = "Content-Type";
		final String AUTHORIZATION = "Authorization";
		
		InputStream input = null;
		String server = "";
		String token = "";
		String projectKey = "";
		String authString = "";
		String authStringEnc = "";
		String baseDir = "";
		String projectID = "";
		String submittedAt = "";
		String status = "";
	    Date dNow = new Date( );
	    SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ssZ");
	    String ActualDate = ft.format(dNow);
		

	try {

		server = s;
		token = "b54848047bb6c95c352fbc1f1704370fe1bb2d98";
		projectKey = p;
		authString = token + ":";
		byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
		authStringEnc = new String(authEncBytes);
		baseDir = dir;


	} finally {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	try {    	
        
		do { 
			
			//Project ID
			URL url = new URL(server + "/api/projects/search?projects=" + projectKey);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty(CONTENT_TYPE, "application/json");
			con.setRequestProperty(AUTHORIZATION, "Basic " + authStringEnc);
			 		
			BufferedReader read = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line = read.readLine();
			String json = "";
			  		
			while(line!=null) {
				json += line;
				line = read.readLine();
			}

			read.close();
						
			con.disconnect();
					
			JSONParser parser = new JSONParser();
					   
			Object object = parser.parse(json);

			JSONObject jsonObject =  (JSONObject) object;

			JSONArray components = (JSONArray) jsonObject.get("components");
				    
			Iterator i = components.iterator();

			JSONObject obj =  (JSONObject) i.next();
			projectID = String.valueOf(obj.get("id"));
			
			//Background Tasks
			url = new URL(server + "/api/ce/activity?onlyCurrents=true&componentId=" + projectID);
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty(CONTENT_TYPE, "application/json");
			con.setRequestProperty(AUTHORIZATION, "Basic " + authStringEnc);
			 		
			read = new BufferedReader(new InputStreamReader(con.getInputStream()));
			line = read.readLine();
			json = "";
			  		
			while(line!=null) {
				json += line;
				line = read.readLine();
			}

			read.close();
						
			con.disconnect();
					
			parser = new JSONParser();
					   
			object = parser.parse(json);

			jsonObject =  (JSONObject) object;

			JSONArray tasks = (JSONArray) jsonObject.get("tasks");
				    
			i = tasks.iterator();

			obj =  (JSONObject) i.next();
			submittedAt = String.valueOf(obj.get("submittedAt"));
			status = String.valueOf(obj.get("status"));
			
		} while(ActualDate.compareTo(submittedAt) >= 0 && status.equals("SUCCESS"));
			
		logger.info("OK");
		
	} catch(MalformedURLException ex) {
		logger.error(ex.getMessage());
	} catch (FileNotFoundException e) {
		logger.error(e.getMessage());
	} catch(IOException ioex) {
		logger.error(ioex.getMessage());
	} 
	
	
	}
}      
