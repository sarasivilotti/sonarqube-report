package com.spindox.SonarReport;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;

public class ProducerXML {
	
	private String rate = "";
	private static final String BLOCKER = "Blocker";
	private static final String CRITICAL = "Critical";
	private static final String MAJOR = "Major";
	private static final String MINOR = "Minor";
	private static final String INFO = "Info";
	private static final String TOTAL = "Total";
	
	final Logger logger = LoggerFactory.getLogger(Report.class);
	

   public void produceXML(String Date, String Project, String Analyses, String V, String Lines, int[][] issues, String rate, String dir) {

      try {
         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.newDocument();
         
         // root element
         Element rootElement = doc.createElement("Report");
         doc.appendChild(rootElement);

         // TodayDate element
         Element TodayDate = doc.createElement("TodayDate");
         TodayDate.setTextContent(Date);
         rootElement.appendChild(TodayDate);
         
         // ProjectName element
         Element ProjectName = doc.createElement("ProjectName");
         ProjectName.setTextContent(Project);
         rootElement.appendChild(ProjectName);
         
         // AnalysesDate element
         Element AnalysesDate = doc.createElement("AnalysesDate");
         AnalysesDate.setTextContent(Analyses);
         rootElement.appendChild(AnalysesDate);
         
         // Version element
         Element Version = doc.createElement("Version");
         Version.setTextContent(V);
         rootElement.appendChild(Version);
         
         // LOC element
         Element LOC = doc.createElement("LOC");
         LOC.setTextContent(Lines);
         rootElement.appendChild(LOC);
         
         // Bugs element
         Element Bugs = doc.createElement("Bugs");
         rootElement.appendChild(Bugs);
         
         Element BBlocker = doc.createElement(BLOCKER);
         BBlocker.setTextContent(String.valueOf(issues[0][0]));
         Bugs.appendChild(BBlocker);
         
         Element BCritical = doc.createElement(CRITICAL);
         BCritical.setTextContent(String.valueOf(issues[0][1]));
         Bugs.appendChild(BCritical);
         
         Element BMajor = doc.createElement(MAJOR);
         BMajor.setTextContent(String.valueOf(issues[0][2]));
         Bugs.appendChild(BMajor);
         
         Element BMinor = doc.createElement(MINOR);
         BMinor.setTextContent(String.valueOf(issues[0][3]));
         Bugs.appendChild(BMinor);
         
         Element BInfo = doc.createElement(INFO);
         BInfo.setTextContent(String.valueOf(issues[0][4]));
         Bugs.appendChild(BInfo);
         
         Element BTotal = doc.createElement(TOTAL);
         int TotBugs = issues[0][0] + issues[0][1] + issues[0][2] + issues[0][3] + issues[0][4];
         BTotal.setTextContent(String.valueOf(TotBugs));
         Bugs.appendChild(BTotal);
         
         Element BugsRating = doc.createElement("BugsRating");
         String BRate = getRating(issues[0][0], issues[0][1], issues[0][2], issues[0][3], TotBugs);
         BugsRating.setTextContent(BRate);
         Bugs.appendChild(BugsRating); 
                           
         // Vulnerabilities element
         Element Vulnerabilities = doc.createElement("Vulnerabilities");
         rootElement.appendChild(Vulnerabilities);
         
         Element VBlocker = doc.createElement(BLOCKER);
         VBlocker.setTextContent(String.valueOf(issues[1][0]));
         Vulnerabilities.appendChild(VBlocker);
         
         Element VCritical = doc.createElement(CRITICAL);
         VCritical.setTextContent(String.valueOf(issues[1][1]));
         Vulnerabilities.appendChild(VCritical);
         
         Element VMajor = doc.createElement(MAJOR);
         VMajor.setTextContent(String.valueOf(issues[1][2]));
         Vulnerabilities.appendChild(VMajor);
         
         Element VMinor = doc.createElement(MINOR);
         VMinor.setTextContent(String.valueOf(issues[1][3]));
         Vulnerabilities.appendChild(VMinor);
         
         Element VInfo = doc.createElement(INFO);
         VInfo.setTextContent(String.valueOf(issues[1][4]));
         Vulnerabilities.appendChild(VInfo);
         
         Element VTotal = doc.createElement(TOTAL);
         int TotVulnerabilities = issues[1][0] + issues[1][1] + issues[1][2] + issues[1][3] + issues[1][4];
         VTotal.setTextContent(String.valueOf(TotVulnerabilities));
         Vulnerabilities.appendChild(VTotal);
         
         Element VulnerabilitiesRating = doc.createElement("VulnerabilitiesRating");
         String VRate = getRating(issues[1][0], issues[1][1], issues[1][2], issues[1][3], TotVulnerabilities);
         VulnerabilitiesRating.setTextContent(VRate);
         Vulnerabilities.appendChild(VulnerabilitiesRating); 
                  
         // CodeSmells element
         Element CodeSmells = doc.createElement("CodeSmells");
         rootElement.appendChild(CodeSmells);
         
         Element CBlocker = doc.createElement(BLOCKER);
         CBlocker.setTextContent(String.valueOf(issues[2][0]));
         CodeSmells.appendChild(CBlocker);
         
         Element CCritical = doc.createElement(CRITICAL);
         CCritical.setTextContent(String.valueOf(issues[2][1]));
         CodeSmells.appendChild(CCritical);
         
         Element CMajor = doc.createElement(MAJOR);
         CMajor.setTextContent(String.valueOf(issues[2][2]));
         CodeSmells.appendChild(CMajor);
         
         Element CMinor = doc.createElement(MINOR);
         CMinor.setTextContent(String.valueOf(issues[2][3]));
         CodeSmells.appendChild(CMinor);
         
         Element CInfo = doc.createElement(INFO);
         CInfo.setTextContent(String.valueOf(issues[2][4]));
         CodeSmells.appendChild(CInfo);
         
         Element CTotal = doc.createElement(TOTAL);
         int TotCodeSmells = issues[2][0] + issues[2][1] + issues[2][2] + issues[2][3] + issues[2][4];
         CTotal.setTextContent(String.valueOf(TotCodeSmells));
         CodeSmells.appendChild(CTotal);
         
         Element CSRating = doc.createElement("CSRating");
         String CRate = rate;
         CSRating.setTextContent(CRate);
         CodeSmells.appendChild(CSRating); 
           
         // Total Element 
         int sum = 0;
         
         Element Tot = doc.createElement(TOTAL);
         rootElement.appendChild(Tot);
         
         Element Blocker = doc.createElement("TotalBlocker");
         Blocker.setTextContent(String.valueOf(issues[0][0] + issues[1][0] + issues[2][0]));
         Tot.appendChild(Blocker);
         sum += issues[0][0] + issues[1][0] + issues[2][0];
         
         Element Critical = doc.createElement("TotalCritical");
         Critical.setTextContent(String.valueOf(issues[0][1] + issues[1][1] + issues[2][1]));
         Tot.appendChild(Critical);
         sum += issues[0][1] + issues[1][1] + issues[2][1];
         
         Element Major = doc.createElement("TotalMajor");
         Major.setTextContent(String.valueOf(issues[0][2] + issues[1][2] + issues[2][2]));
         Tot.appendChild(Major);
         sum += issues[0][2] + issues[1][2] + issues[2][2];
         
         Element Minor = doc.createElement("TotalMinor");
         Minor.setTextContent(String.valueOf(issues[0][3] + issues[1][3] + issues[2][3]));
         Tot.appendChild(Minor);
         sum += issues[0][3] + issues[1][3] + issues[2][3];
         
         Element Info = doc.createElement("TotalInfo");
         Info.setTextContent(String.valueOf(issues[0][4] + issues[1][4] + issues[2][4]));
         Tot.appendChild(Info);
         sum += issues[0][4] + issues[1][4] + issues[2][4];
         
         Element Total = doc.createElement(TOTAL);
         Total.setTextContent(String.valueOf(sum));
         Tot.appendChild(Total); 

         // Write content into xml file
         TransformerFactory transformerFactory = TransformerFactory.newInstance();
         javax.xml.transform.Transformer transformer = transformerFactory.newTransformer();
         DOMSource source = new DOMSource(doc);
         StreamResult result = new StreamResult(new File(dir + "/report.xml"));
         transformer.transform(source, result);
                  
      } catch (Exception e) {
    	  logger.error(e.getMessage());
      }
   }
   
   public String getRating(int blocker, int critical, int major, int minor, int total) {
		
		if(total != 0 && blocker != 0 && blocker >= 1) {
			return "E";
		} else if (total != 0 && critical != 0 && critical >= 1) {
			return "D";
		} else if (total != 0 && major != 0 && major >= 1) {
			return "C";
		} else if (total != 0 && minor != 0 && minor >= 1) {
			return "B";
		} else if (total == 0) {
			return "A";
		}
				
		return rate;		
	}
	
}