package com.spindox.SonarReport;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerException;
import javax.xml.transform.Source;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class ProducerXSL {

    public void ProduceXSL(String dir) throws IOException, TransformerException {

    	//Setup input and output files
        File xmlfile = new File(dir + "/report.xml");
        File xsltfile = new File(dir + "/report.xsl");
        File fofile = new File(dir + "/report.fo");
        
    	//Setup output
        OutputStream out = new java.io.FileOutputStream(fofile);
        
        try {
            //Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xsltfile));

            //Setup input for XSLT transformation
            Source src = new StreamSource(xmlfile);

            //Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new StreamResult(out);

            //Start XSLT transformation and FOP processing
            transformer.transform(src, res);
            
                  
        } finally {
            out.close();
        }
    }

}