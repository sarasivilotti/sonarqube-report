package com.spindox.SonarReport;

import org.apache.commons.codec.binary.Base64;

import jxl.write.WriteException;
import java.net.*;
import java.text.SimpleDateFormat;
import java.io.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Report {

	public static void main(String[] args) throws WriteException, IOException, ParseException {
		
		final Logger logger = LoggerFactory.getLogger(Report.class);
		
		final String CONTENT_TYPE = "Content-Type";
		final String AUTHORIZATION = "Authorization";
		
		InputStream input = null;
		String server = "";
		String token = "";
		String projectKey = "";
		String authString = "";
		String authStringEnc = "";
		String baseDir = "";
		int[][] issues =  {{ 0, 0, 0, 0, 0}, { 0, 0, 0, 0, 0}, { 0, 0, 0, 0, 0}};
		String LOC = "";
	    Date dNow = new Date( );
	    SimpleDateFormat ft = 
	    new SimpleDateFormat ("yyyy/MM/dd");
	    String ActualDate = ft.format(dNow);
		

	try {

		server = args[0];
		token = "b54848047bb6c95c352fbc1f1704370fe1bb2d98";
		projectKey = args[1];
		authString = token + ":";
		byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
		authStringEnc = new String(authEncBytes);
		baseDir = args[2];


	} finally {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	BackgroundTask bt = new BackgroundTask();
	bt.ReportSuccess(server, projectKey, baseDir);
	
	try {
		
		String[] severities = { "BLOCKER", "CRITICAL", "MAJOR", "MINOR", "INFO" };
		String[] type = { "BUG", "VULNERABILITY", "CODE_SMELL" };
		
		// Ciclo su severities
		for (int s = 0; s < 5; s++) {
			
			// Ciclo su types
			for(int t = 0; t < 3; t++) {	
		
		//Total Issue by Severity and Type		
		URL url = new URL(server + "/api/issues/search?componentKeys=" + projectKey + "&resolved=false&severities=" + severities[s] + "&types="+ type[t]);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
 		con.setRequestMethod("GET");
 		con.setRequestProperty(CONTENT_TYPE, "application/json");
 		con.setRequestProperty(AUTHORIZATION, "Basic " + authStringEnc);
 		
 		BufferedReader read = new BufferedReader(new InputStreamReader(con.getInputStream()));
  		String line = read.readLine();
  		String json = "";
  		
  		while(line!=null) {
    		json += line;
    		line = read.readLine();
  		}

  		read.close();
			
		con.disconnect();
		
		JSONParser parser = new JSONParser();
		   
	    Object object = parser.parse(json);

	    JSONObject jsonObject =  (JSONObject) object;

	    String obj = String.valueOf(jsonObject.get("total"));
        long total = Long.parseLong(obj);
	    issues[t][s] = (int) total;
		
			}
		}
		
		//Line Of Code
		URL url = new URL(server + "/api/measures/component?component=" + projectKey + "&metricKeys=ncloc");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
 		con.setRequestMethod("GET");
 		con.setRequestProperty(CONTENT_TYPE, "application/json");
 		con.setRequestProperty(AUTHORIZATION, "Basic " + authStringEnc);
 		
 		BufferedReader read = new BufferedReader(new InputStreamReader(con.getInputStream()));
  		String line = read.readLine();
  		String json = "";
  		
  		while(line!=null) {
    		json += line;
    		line = read.readLine();
  		}

  		read.close();
			
		con.disconnect();
		
		JSONParser parser = new JSONParser();
		   
	    Object object = parser.parse(json);

	    JSONObject jsonObject =  (JSONObject) object;

	    JSONObject component = (JSONObject) jsonObject.get("component");
	    JSONArray measures = (JSONArray) component.get("measures");
	    
	    Iterator i = measures.iterator();

        JSONObject obj =  (JSONObject) i.next();
        LOC = String.valueOf(obj.get("value"));
    	
		//Analyses Date
		url = new URL(server + "/api/project_analyses/search?project="+ projectKey);
		con = (HttpURLConnection) url.openConnection();
 		con.setRequestMethod("GET");
 		con.setRequestProperty(CONTENT_TYPE, "application/json");
 		con.setRequestProperty(AUTHORIZATION, "Basic " + authStringEnc);
 		
 		read = new BufferedReader(new InputStreamReader(con.getInputStream()));
  		line = read.readLine();
  		json = "";
  		
  		while(line!=null) {
    		json += line;
    		line = read.readLine();
  		}

  		read.close();
			
		con.disconnect();
		
		parser = new JSONParser();
		   
	    object = parser.parse(json);

	    jsonObject =  (JSONObject) object;

	    JSONArray analyses = (JSONArray) jsonObject.get("analyses");
	    
	    Iterator a = analyses.iterator();

        obj =  (JSONObject) a.next();
        String AnalysesDate = String.valueOf(obj.get("date"));
        AnalysesDate = AnalysesDate.substring(0,10) + "  " + AnalysesDate.substring(11,16);
        
		//Version
		url = new URL(server + "/api/project_analyses/search?project=" + projectKey + "&category=VERSION");
		con = (HttpURLConnection) url.openConnection();
 		con.setRequestMethod("GET");
 		con.setRequestProperty(CONTENT_TYPE, "application/json");
 		con.setRequestProperty(AUTHORIZATION, "Basic " + authStringEnc);
 		
 		read = new BufferedReader(new InputStreamReader(con.getInputStream()));
  		line = read.readLine();
  		json = "";
  		
  		while(line!=null) {
    		json += line;
    		line = read.readLine();
  		}

  		read.close();
			
		con.disconnect();
		
		parser = new JSONParser();		   
	    object = parser.parse(json);
	    jsonObject =  (JSONObject) object;
	    analyses = (JSONArray) jsonObject.get("analyses");	    
	    i = analyses.iterator();
        obj =  (JSONObject) i.next();
        JSONArray events = (JSONArray) obj.get("events");        
	    i = events.iterator();
        obj =  (JSONObject) i.next();
        String Version = String.valueOf(obj.get("name"));
        
        // Maintainability Rating
        url = new URL(server + "/api/project_badges/measure?project=" + projectKey + "&metric=sqale_rating");
		con = (HttpURLConnection) url.openConnection();
 		con.setRequestMethod("GET");
 		con.setRequestProperty(CONTENT_TYPE, "image/svg+xml");
 		con.setRequestProperty(AUTHORIZATION, "Basic " + authStringEnc);
 		
 		read = new BufferedReader(new InputStreamReader(con.getInputStream()));
  		line = read.readLine();
  		String svg = "";
  		
  		while(line!=null) {
    		svg += line;
    		line = read.readLine();
  		}
  		
  		String rate = svg.substring(877, 878);

  		read.close();

		con.disconnect();
		
		//Project Name
		url = new URL(server + "/api/projects/search?projects=" + projectKey);
		con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty(CONTENT_TYPE, "application/json");
		con.setRequestProperty(AUTHORIZATION, "Basic " + authStringEnc);
		 		
		read = new BufferedReader(new InputStreamReader(con.getInputStream()));
		line = read.readLine();
		json = "";
		  		
		while(line!=null) {
			json += line;
			line = read.readLine();
		}

		read.close();
					
		con.disconnect();
				
		parser = new JSONParser();
				   
		object = parser.parse(json);

		jsonObject =  (JSONObject) object;

		JSONArray components = (JSONArray) jsonObject.get("components");
			    
		a = components.iterator();

		obj =  (JSONObject) a.next();
		String projectName = String.valueOf(obj.get("name"));
				
        //Write Excel File
    	/*Writer report = new Writer();
        report.setOutputFile("C:/ReportSonar/ReportSonar.xls");
        report.write(issues, projectName);       
        logger.info("Check the result file under C:/ReportSonar/ReportSonar.xls");*/
        
        try {
        	
        	ProducerXML xml = new ProducerXML();
        	xml.produceXML(ActualDate, projectName, AnalysesDate, Version, LOC, issues, rate, baseDir);
        	ProducerXSL xsl = new ProducerXSL();
        	xsl.ProduceXSL(baseDir);                 	
            
        } catch (Exception e) {
        	logger.error(e.getMessage());
        }


	} catch(MalformedURLException ex) {
		logger.error(ex.getMessage());
	} catch (FileNotFoundException e) {
		logger.error(e.getMessage());
	} catch(IOException ioex) {
		logger.error(ioex.getMessage());
	} 
	
	
	}
}      
