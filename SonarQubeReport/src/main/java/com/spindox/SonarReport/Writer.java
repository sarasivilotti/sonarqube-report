package com.spindox.SonarReport;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class Writer {

    private WritableCellFormat timesBold;
    private WritableCellFormat times;
    private String inputFile;

    public void setOutputFile(String inputFile) {
    	this.inputFile = inputFile;
    }

    public void write(int[][] issues, String projectName) throws IOException, WriteException {
        File file = new File(inputFile);
        WorkbookSettings wbSettings = new WorkbookSettings();

        wbSettings.setLocale(new Locale("en", "EN"));

        WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
        workbook.createSheet("Report", 0);
        WritableSheet excelSheet = workbook.getSheet(0);
        createLabel(excelSheet);
        createContent(excelSheet);
        
        addCaption(excelSheet, 0, 0, projectName);
        
		// Ciclo su severities
		for (int s = 0; s < 5; s++) {
			
			// Ciclo su types
			for(int t = 0; t < 3; t++) {
				
				addNumber(excelSheet, t + 1, s + 1, issues[t][s]);
        }}

        workbook.write();
        workbook.close();
    }

    private void createLabel(WritableSheet sheet)throws WriteException {
        WritableFont times12pt = new WritableFont(WritableFont.TIMES, 12);
        times = new WritableCellFormat(times12pt);
        times.setWrap(true);

        // Create a bold font
        WritableFont times12ptBold = new WritableFont(
                WritableFont.TIMES, 12, WritableFont.BOLD);
        timesBold = new WritableCellFormat(times12ptBold);
        timesBold.setWrap(true);

        CellView cv = new CellView();
        cv.setFormat(times);
        cv.setFormat(timesBold);
        cv.setSize(4000);
        for(int i = 1; i < 4; i++ ) {
        	sheet.setColumnView(i, cv); 
        }
        cv.setSize(8000);
        sheet.setColumnView(0, cv); 

        // Write headers
        addCaption(sheet, 1, 0, "Bug");
        addCaption(sheet, 2, 0, "Vulnerabilities");
        addCaption(sheet, 3, 0, "Code Smells");
    }

    private void createContent(WritableSheet sheet) throws WriteException {
        addLabel(sheet, 0, 1, "Blocker");
        addLabel(sheet, 0, 2, "Critical");
        addLabel(sheet, 0, 3, "Major");
        addLabel(sheet, 0, 4, "Minor");
        addLabel(sheet, 0, 5, "Info");
    }

    private void addCaption(WritableSheet sheet, int column, int row, String s) throws WriteException {
        Label label;
        label = new Label(column, row, s, timesBold);
        sheet.addCell(label);
    }

    public void addNumber(WritableSheet sheet, int column, int row, Integer integer) throws WriteException {
        Number number;
        number = new Number(column, row, integer, times);
        sheet.addCell(number);
    }

    private void addLabel(WritableSheet sheet, int column, int row, String s) throws WriteException {
        Label label;
        label = new Label(column, row, s, times);
        sheet.addCell(label);
    }

}