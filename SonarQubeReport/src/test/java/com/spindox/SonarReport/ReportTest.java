package com.spindox.SonarReport;

import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.assertTrue;

public class ReportTest {
	
	private ProducerXML xml;

	@Before
	public void setUp() {
        xml = new ProducerXML();
    }

    @Test
    public void testGetRatingA() {
    	String rating = xml.getRating(0, 0, 0, 0, 0);
    	assertTrue(rating.equalsIgnoreCase("A"));
    }
    
    @Test
    public void testGetRatingB() {
    	String rating = xml.getRating(0, 0, 0, 4, 4);
    	assertTrue(rating.equalsIgnoreCase("B"));
    }
    
    @Test
    public void testGetRatingC() {
    	String rating = xml.getRating(0, 0, 2, 4, 6);
    	assertTrue(rating.equalsIgnoreCase("C"));
    }
    
    @Test
    public void testGetRatingD() {
    	String rating = xml.getRating(0, 1, 2, 4, 7);
    	assertTrue(rating.equalsIgnoreCase("D"));
    }
    
    @Test
    public void testGetRatingE() {
    	String rating = xml.getRating(1, 1, 2, 4, 8);
    	assertTrue(rating.equalsIgnoreCase("E"));
    }
    
}